<?php

class Statistics_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function selectServer($server) {
        $records = $this->db->select('statistics', array('data_value', 'data_label'), array('s_system' => $server));
        $temp = array();
        for ($index = 0; $index < count($records); $index++) {
            $temp[$records[$index]['data_label']] = $records[$index]['data_value'];
        }
        ksort($temp);
        $min = $this->searchLowestValue($temp);
        $max = $this->searchHighestValue($temp);
        $avg = $this->searchAverageValue($temp);
        $array_keys = array_keys($temp);
        $array_values = array_values($temp);
        $result = array(
            'label' => $array_keys, 'value' => $array_values
           , 'min' => $min, 'max' => $max, 'average' => $avg
        );
        return json_encode($result);
    }

    function searchLowestValue($dataArray) {
        $v = min($dataArray);
        $k = array_search($v, $dataArray);
        return array('label' => $k, 'value' => $v);
    }

    function searchHighestValue($dataArray) {
        $v = max($dataArray);
        $k = array_search($v, $dataArray);
        return array('label' => $k, 'value' => $v);
    }

    function searchAverageValue($dataArray) {
        $v = array_sum($dataArray) / count($dataArray);
        return array('label' => 'avarage', 'value' => $v);
    }

}
